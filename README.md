# Dossy

Out of the fires of the first MacDevOps YVR Hack Night comes Dossy. A small, but mighty, utility to ingest, edit and then export plist files as .mobileconfig profiles.

Note that this project achieved something at least marginally useful in 24 hours while Team Python did not... Just saying...

## Use

Open Dossy and then point it to a .mobileconfig file. Dossy is able to read both signed and unsigned profiles. When completed Dossy will allow you to edit these files and then export them to a .mobileconfig again either signed or unsigned.

## Note

Dossy is currently in a post-alpha form and while it can import files and allow you to edit them, it is unable to export at this time. However, that should come shortly.

## Acknowledgements

A big thanks to @matx for putting together a wonderful conference. Also thanks to all of Team Swift for hanging out during Hack Night and drinking beers.