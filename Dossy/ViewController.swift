//
//  ViewController.swift
//  Dossy
//
//  Created by Joel Rennich on 6/6/17.
//  Copyright © 2017 MacDevOpsVYR. All rights reserved.
//

import Cocoa
import SecurityInterface.SFCertificatePanel
import SecurityInterface.SFChooseIdentityPanel

class ViewController: NSViewController {

    @IBOutlet var text: NSTextView!

    @IBOutlet weak var outline: NSOutlineView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

    }

    override func viewDidAppear() {
        super.viewDidAppear()
        
        if let doc = document as? Document {
            text.string = doc.text
            if doc.content != nil {
                outline.dataSource = doc as! NSOutlineViewDataSource
            }
        }

    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    func dumpData() -> Dictionary<String, Any> {
        return Dictionary<String, Any>()
    }


}

