//
//  Document.swift
//  Dossy
//
//  Created by Joel Rennich on 6/6/17.
//  Copyright © 2017 MacDevOpsVYR. All rights reserved.
//

import Cocoa
import SecurityInterface.SFCertificatePanel

class Document: NSDocument, NSOutlineViewDataSource {

    var text = "Boom"
    var content: AnyObject? = nil
    var plistObject: BAVPlistNode? = nil
    var unsigned = Data()

    var rootNode: NSDictionary? = nil

    override init() {
        super.init()
        // Add your subclass-specific initialization here.
    }

    override class func autosavesInPlace() -> Bool {
        return true
    }

    override func makeWindowControllers() {
        // Returns the Storyboard that contains your Document window.
        let storyboard = NSStoryboard(name: "Main", bundle: nil)
        let windowController = storyboard.instantiateController(withIdentifier: "Document Window Controller") as! NSWindowController
        self.addWindowController(windowController)
    }

    override func data(ofType typeName: String) throws -> Data {

        // CHEAT: only save original data

        var saveData = Data()

        do {
        saveData = try! PropertyListSerialization.data(fromPropertyList: unsigned, format: .xml, options: .init())
        } catch {
            throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
        }
        return saveData

    }

    override func read(from data: Data, ofType typeName: String) throws {

        if data == nil {
            return
        }

        // set up some decoding bits

        var err: OSStatus? = nil
        var decoder: CMSDecoder? = nil
        var encrypted: DarwinBoolean = false

        var temp: CFData? = nil

        // do the decode

        err = CMSDecoderCreate(&decoder)
        err = CMSDecoderUpdateMessage(decoder!, (data as NSData).bytes, (data.count))
        err = CMSDecoderFinalizeMessage(decoder!)
        err = CMSDecoderIsContentEncrypted(decoder!, &encrypted)
        err = CMSDecoderCopyContent(decoder!, &temp)

        // if the decode fails, it wasn't signed, so go back to the raw bits

        if err != 0 {
            unsigned = data
        } else {
            unsigned = temp as! Data
        }

        text = String(data: unsigned as! Data, encoding: .utf8)!

        // now to covert to serialized plist

        do {
            content = try PropertyListSerialization.propertyList(from: unsigned, options: .mutableContainers, format: nil) as AnyObject
            plistObject = BAVPlistNode(from: content, key: "Root")

            text = String(describing: (content as! NSDictionary?)?.allValues)

            //dumpData()

        } catch let error {
            // error here
            content = nil
            throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
        }

        // Insert code here to read your document from the given data of the specified type. If outError != nil, ensure that you create and set an appropriate error when returning false.
        // You can also choose to override readFromFileWrapper:ofType:error: or readFromURL:ofType:error: instead.
        // If you override either of these, you should also override -isEntireFileLoaded to return false if the contents are lazily loaded.
        // throw NSError(domain: NSOSStatusErrorDomain, code: unimpErr, userInfo: nil)
    }

    // MARK: Utility functions

    func dumpData() -> NSDictionary? {

        let dict = plistObject as! Data
        NSLog(String(describing: dict))

        return nil
    }


    // MARK: Outline view functions

    func outlineView(_ outlineView: NSOutlineView, numberOfChildrenOfItem item: Any?) -> Int {
        if (item == nil) {
            return 1
        }

        let tempItem = item as! BAVPlistNode

        return tempItem.children.count
    }

    func outlineView(_ outlineView: NSOutlineView, child index: Int, ofItem item: Any?) -> Any {

        if (item == nil) {
            return plistObject!
        }

        let tempItem = item as! BAVPlistNode
        return tempItem.children[index]
    }

    func outlineView(_ outlineView: NSOutlineView, isItemExpandable item: Any) -> Bool {
        return item == nil || (item as AnyObject).collection
    }

    // this reads the values

    // Using "Cell Based" content mode (specify this in IB)
    func outlineView(_ outlineView: NSOutlineView, objectValueFor tableColumn: NSTableColumn?, byItem item: Any?) -> Any? {

        let id = tableColumn?.identifier ?? ""

        let tempItem = item as! BAVPlistNode

        switch id {
            case "key":
                return tempItem.key
            case "value":

//                if tempItem.type == "Boolean" {
//                    let pop = NSComboBox()
//                    pop.addItems(withObjectValues: ["true", "false"])
//                    pop.selectItem(withObjectValue: tempItem.value)
//
//                    var cell = tableColumn?.dataCell(forRow: (tableColumn?.tableView?.selectedRow)!)
//                    cell = NSComboBoxCell().
//                    return pop
//                }


                if ((tempItem.value as? String)?.contains("Example:"))! {
                    let myMutableString = NSMutableAttributedString(string:(tempItem.value as! String))
                    myMutableString.addAttribute(NSForegroundColorAttributeName, value: NSColor.red, range: NSRange(location: 0, length: 7))
                    return myMutableString
                }
                return tempItem.value
            case "type":
                return tempItem.type
            default:
                return nil
        }
    }

    // this sets the values

    func outlineView(_ outlineView: NSOutlineView, setObjectValue object: Any?, for tableColumn: NSTableColumn?, byItem item: Any?) {

        let id = tableColumn?.identifier ?? ""

        let tempItem = item as! BAVPlistNode

        switch id {
        case "key":
            tempItem.key = object as! String
        case "value":
            tempItem.value = object as! NSObject
        case "type":
            tempItem.type = object as! String
        default: break
        }
    }
}

