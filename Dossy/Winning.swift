//
//  Winning.swift
//  Dossy
//
//  Created by Joel Rennich on 6/6/17.
//  Copyright © 2017 MacDevOpsVYR. All rights reserved.
//

import Foundation
import Cocoa

class Winning: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()
        
        if let backgroundWindow = self.window {
            let mainDisplayRect = NSScreen.main()?.frame
            backgroundWindow.contentRect(forFrameRect: mainDisplayRect!)
            backgroundWindow.setFrame((NSScreen.main()?.frame)!, display: true)
            backgroundWindow.setFrameOrigin((NSScreen.main()?.frame.origin)!)
            backgroundWindow.level = Int(CGWindowLevelForKey(.maximumWindow) - 1 )
        }
    }

}
